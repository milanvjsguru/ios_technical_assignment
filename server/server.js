const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        searchEngines: [
            {
                id: 1,
                name: "Google",
                address: "www.google.com"
            },
            {
                id: 2,
                name: "Yahoo",
                address: "www.yahoo.com"
            },
            {
                id: 3,
                name: "Bing",
                address: "www.bing.com"
            },

        ]
    }));
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
});